fn main() {
    println!("cargo:rustc-link-lib=gmp");
    println!("cargo:rustc-link-lib=glib-2.0");
    println!("cargo:rustc-link-lib=pbc");
    println!("cargo:rustc-link-lib=bswabe");
    println!("cargo:rustc-link-lib=crypto"); // for SHA1 function
}
