# Cpabe Cli

A command line tool for Attribute Based Encryption (ABE). It makes use of the [Bswabe library](https://gitlab.com/asclepios-rust/c-libs/libbswabe-sys).


### Disclaimer

This is experimental software! Do not use it or the Bswabe library in a production environment!


### Build

1. You need to follow the build instructions on the Bswabe project (v.s.) first.
2. The Bswabe project needs to be in the same directory as this project:
 - parent director
   - cpabe-cli
     - Cargo.toml
     - README.md
     - src
       - ...
   - libbswabe-sys
     - Cargo.toml
     - README.md
     - src
       - ...
3. Run `cargo build`.


### Usage

You can get a full usage message with `cargo run -- --help`.

##### create a master keypair

```sh
cargo run -- --generate-master-keypair --master-key msk --public-key pubk
```

##### create a user key

```sh
cargo run -- --generate-user-key --master-key msk --public-key pubk --user-key usk
```