/*
 * Cpabe CLI - A command line tool for Attribute Based Encryption (ABE)
 * Copyright (C) 2022 Ilka Elisa Marie Schulz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#[macro_use]
extern crate clap;
use clap::App;
use std::ffi::CString;
use std::fs::File;
use std::io::Write;
use std::os::raw::c_char;

fn main() {
    // The YAML file is found relative to the current file, similar to how modules are found
    let yaml = load_yaml!("cli_options.yml");
    let mut app = App::from_yaml(yaml);
    let matches = app.clone().get_matches();

    if matches.is_present("help") {
        app.print_help().expect("could not print help");
        println!("");
    } else if matches.is_present("version") {
        println!("{} {}", crate_name!(), crate_version!());
    } else if matches.is_present("generate_master_keypair") {
        // all parameters are guaranteed to be defined due to the "require" statement in the YAML file
        gen_msk(
            matches
                .value_of("master_key")
                .expect("no master key file set")
                .to_string(),
            matches
                .value_of("public_key")
                .expect("no public key file set")
                .to_string(),
            matches.is_present("dry_run"),
        );
    } else if matches.is_present("generate_user_key") {
        // all parameters are guaranteed to be defined due to the "require" statement in the YAML file
        gen_uk(
            matches
                .value_of("master_key")
                .expect("no master key file set")
                .to_string(),
            matches
                .value_of("public_key")
                .expect("no public key file set")
                .to_string(),
            matches
                .value_of("user_key")
                .expect("no user key file set")
                .to_string(),
            matches
                .value_of("attributes")
                .expect("no attributes set")
                .to_string(),
            matches.is_present("dry_run"),
        );
    } else {
        app.print_help().expect("could not print help");
        println!("");
    }
}

fn gen_msk(m_file: String, p_file: String, dry_run: bool) {
    // create master keypair
    let mut pubk: *mut libbswabe_sys::bswabe_pub_t = std::ptr::null_mut();
    let mut msk: *mut libbswabe_sys::bswabe_msk_t = std::ptr::null_mut();
    unsafe {
        libbswabe_sys::bswabe_setup(std::ptr::addr_of_mut!(pubk), std::ptr::addr_of_mut!(msk));
    }
    if dry_run {
        return;
    }

    // write public key to file
    let pubk = unsafe { libbswabe_sys::bswabe_pub_serialize(pubk) };
    let mut file = File::create(p_file).expect("could not create public key file");
    file.write(unsafe {
        std::slice::from_raw_parts(
            (*pubk).data,
            (*pubk)
                .len
                .try_into()
                .expect("public key data size cannot be converted to usize type"),
        )
    })
    .expect("could not write to public key file");

    let msk = unsafe { libbswabe_sys::bswabe_msk_serialize(msk) };
    let mut file = File::create(m_file).expect("could not create master key file");
    file.write(unsafe {
        std::slice::from_raw_parts(
            (*msk).data,
            (*msk)
                .len
                .try_into()
                .expect("master key data size cannot be converted to usize type"),
        )
    })
    .expect("could not write to master key file");
}
fn gen_uk(m_file: String, p_file: String, u_file: String, attr: String, dry_run: bool) {
    // read public key from file
    let mut pubk = std::fs::read(p_file).expect("could not read public key from file"); // mut because that C library requires it, sigh...
    let mut pubk: libbswabe_sys::GByteArray = libbswabe_sys::GByteArray {
        // mut because that C library requires it, sigh...
        data: pubk.as_mut_ptr(),
        len: pubk
            .len()
            .try_into()
            .expect("public key data size cannot be converted to u32 type"),
    };
    let pubk = unsafe { libbswabe_sys::bswabe_pub_unserialize(std::ptr::addr_of_mut!(pubk), 0) };

    // read master key from file
    let mut msk = std::fs::read(m_file).expect("could not read master key from file"); // mut because that C library requires it, sigh...
    let mut msk: libbswabe_sys::GByteArray = libbswabe_sys::GByteArray {
        // mut because that C library requires it, sigh...
        data: msk.as_mut_ptr(),
        len: msk
            .len()
            .try_into()
            .expect("master key data size cannot be converted to u32 type"),
    };
    let msk =
        unsafe { libbswabe_sys::bswabe_msk_unserialize(pubk, std::ptr::addr_of_mut!(msk), 0) };

    // generate attributes array
    let attr = get_attributes_from_string(&attr);

    // see https://stackoverflow.com/questions/42717473/passing-vecstring-from-rust-to-char-in-c
    let cstr_vector: Vec<CString> = attr // a vector of C strings
        .iter()
        .map(|arg| CString::new(arg.as_str()).unwrap())
        .collect();
    let mut p_vector: Vec<_> = cstr_vector // a vector of pointers to C strings
        .iter()
        .map(|arg| arg.as_ptr() as *mut c_char) // this is really unsafe but the C library expects mutability for no reason
        .collect();
    p_vector.push(std::ptr::null_mut());

    let p: *mut *mut c_char = p_vector.as_mut_ptr(); // mut because that C library requires it, sigh...

    //generate user key
    let usk =
        unsafe { libbswabe_sys::bswabe_keygen(pubk, msk, p) } as *const libbswabe_sys::bswabe_prv_t;
    if usk == std::ptr::null() {
        panic!("The bswabe library returned a null pointer.");
    }

    if dry_run {
        return;
    }

    // save user key to file
    let usk =
        unsafe { libbswabe_sys::bswabe_prv_serialize(usk as *mut libbswabe_sys::bswabe_prv_t) };
    let mut file = File::create(u_file).expect("could not create user key file");
    file.write(unsafe {
        std::slice::from_raw_parts(
            (*usk).data,
            (*usk)
                .len
                .try_into()
                .expect("user key data size cannot be converted to usize type"),
        )
    })
    .expect("could not write to user key file");
}

fn get_attributes_from_string(attributes: &String) -> Vec<String> {
    let attributes = attributes.split(" ");
    let attributes: Vec<String> = attributes.map(String::from).collect();
    if (&attributes).len() == 0 {
        panic!("The specified attribute list is empty.");
    }
    for attr in (&attributes).into_iter() {
        if (&attr).find(":") == None {
            panic!("The attributes string is not valid (at least one attribute does not follow the \"key:val\" syntax).")
        }
    }
    attributes
}
